//
//  CarlistyError.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 26/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation

struct CarlistyError: Error {
    var message: String? = "Unknown Server Error"
    let detail: String?
    let type: String?
    let errorCode: String?

    init(json: [String: AnyObject]) {
        message = json["message"] as? String
        detail = json["detail"] as? String
        type = json["type"] as? String
        errorCode = json["errorCode"] as? String
    }

    init(message: String, detail: String? = nil, type: String? = nil, errorCode: String? = nil) {
        self.message = message
        self.detail = detail
        self.type = type
        self.errorCode = errorCode
    }
}

extension NSError {
    func asCarlistyError() -> CarlistyError {
        return CarlistyError(message: self.localizedDescription, detail: self.localizedDescription,
                             type: "ERROR", errorCode: String(self.code))
    }
}

extension Error {
    func asCarlistyError() -> CarlistyError {
        if let error = self as? CarlistyResponse {
            return error.asCarlistyError()

        } else if let error = self as? CarlistyError {

            return error

        } else {
            let nserror = self as NSError
            return CarlistyError(message: nserror.localizedDescription, detail:
                nserror.localizedDescription, type: "ERROR", errorCode: String(nserror.code))
        }
    }
}

extension CarlistyResponse {

    func asCarlistyError() -> CarlistyError {
        if case let .failure(_, message) = self {
            return CarlistyError(message: message!)
        } else {
            return CarlistyError(message: "Unknow error")
        }
    }
}
