//
//  Entity.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 05/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import ObjectMapper

class Entity: Mappable {

    func mapping(map: Map) {
    }

    required init?(map: Map) {
    }
}
