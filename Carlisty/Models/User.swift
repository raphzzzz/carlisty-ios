//
//  User.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 25/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import ObjectMapper

class User: Entity {

    var id: String = ""
    var name: String = ""
    var email: String = ""
    var token: String = ""

    override func mapping(map: Map) {
        id <- map["_id"]
        name <- map["name"]
        email <- map["email"]
        token <- map["token"]
    }
}
