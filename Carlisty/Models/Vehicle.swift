//
//  Vehicle.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 16/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import ObjectMapper

class Vehicle: Entity {

    var dealerID: String = ""
    var vin: String = ""
    var stocknumber: String = ""
    var year: String = ""
    var make: String = ""
    var model: String = ""
    var trim: String = ""
    var interiorColor: String = ""
    var exteriorColor: String = ""
    var mileage: String = ""
    var listprice: String = ""
    var views: Int = -1
    var sirvId: String = ""

    override func mapping(map: Map) {
        dealerID <- map["dealerID"]
        vin <- map["vin"]
        stocknumber <- map["stocknumber"]
        year <- map["year"]
        make <- map["make"]
        model <- map["model"]
        trim <- map["trim"]
        interiorColor <- map["interiorColor"]
        exteriorColor <- map["exteriorColor"]
        mileage <- map["mileage"]
        listprice <- map["listprice"]
        views <- map["views"]
        sirvId <- map["sirvId"]
    }
}
