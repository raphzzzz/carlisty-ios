//
//  Dealer.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 05/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import ObjectMapper

class Dealer: Entity {

    var id: String = ""
    var name: String = ""
    var address: String = ""
    var profileImage: String?

    override func mapping(map: Map) {
        id <- map["_id"]
        name <- map["name"]
        address <- map["address"]
        profileImage <- map["profileImage"]
    }
}
