//
//  DetailsViewModel.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 19/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import RxSwift

class DetailsViewModel {

    let carlistyConstants = CarlistyConstants()

    var detailsVc: DetailsViewController?

    var vehicleSelected: Vehicle?

    func setSelectedVehicle(vehicle: Vehicle) {
        self.vehicleSelected = vehicle
    }

    func initViewController(detailsVc: DetailsViewController) {
        self.detailsVc = detailsVc
    }

    func getVehicle360ViewURL() -> String {
        guard let vehicle = vehicleSelected else {
            return ""
        }

        return String(format: carlistyConstants.sirvDomainURL + carlistyConstants.sirv360SpinPostfix,
                      "\(vehicle.views)", vehicle.sirvId)
    }

    func getVehicleMp4URL() -> String {
        guard let vehicle = vehicleSelected else {
            return ""
        }

        return String(format: carlistyConstants.sirvDomainURL + carlistyConstants.sirvMp4Postfix,
                      "\(vehicle.views)", vehicle.sirvId)
    }

    func getThumbImageUrlFromIndex(imageId: String) -> String {
        guard let vehicle = vehicleSelected else {
            return ""
        }

        return String(format: carlistyConstants.sirvDomainImageURL, "\(vehicle.views)", vehicle.sirvId, imageId)
    }
}
