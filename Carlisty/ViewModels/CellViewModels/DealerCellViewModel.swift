//
//  DealerCellViewModel.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 24/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import SDWebImage

class DealerCellViewModel {

    func bindTo(cell: DealerTableViewCell, dealer: Dealer) {
        cell.nameLabel.text = dealer.name
        cell.addressLabel.text = dealer.address

        if let profileImageURL = dealer.profileImage {
            cell.profileImageView.sd_setImage(with: URL(string: profileImageURL),
                                              placeholderImage: UIImage(named: "profile_picture_placeholder.png"))
        }
    }
}
