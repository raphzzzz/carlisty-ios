//
//  HomeViewModel.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 10/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import RxSwift

class DealerViewModel {

    let bag = DisposeBag()

    var dealers: [Dealer] = []

    var dealerSelected: Dealer?

    var isLoading = true

    func loadInformation() -> Observable<Bool> {
        return Observable.create { observer in

            RestAPI().getDealers().asObservable().subscribe(onNext: { (dealers) in
                self.isLoading = false
                self.dealers = dealers

                observer.onNext(true)
                observer.onCompleted()
            }, onError: { (error) in
                self.isLoading = false
                observer.onError(error)
            }).disposed(by: self.bag)

            return Disposables.create()
        }
    }

    func setSelectedDealerWithIndex(index: Int) {
        dealerSelected = dealers[index]
    }
}
