//
//  LoginViewModel.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 25/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import PromiseKit
import RxSwift

class LoginViewModel {

    let bag = DisposeBag()

    var loginViewController: LoginViewController!

    init(loginViewController: LoginViewController) {
        self.loginViewController = loginViewController
    }

    func login(email: String, password: String) {
        if email.count < 5 || !self.isValidEmail(testStr: email) {
            loginViewController.showInformationAlert(message: "A valid email is required")
            return
        }

        if password.count < 2 {
            loginViewController.showInformationAlert(message: "Password cannot be empty")
            return
        }

        loginViewController.loginSubmitButton.showLoading()

        sendLoginRequest(email: email, password: password).asObservable().subscribe(onNext: { user in
            self.loginViewController.loginSubmitButton.hideLoading()

            CachingServices.currentUser = user

            self.loginViewController.performSegue(withIdentifier: "loginSuccess", sender: nil)
        }, onError: { error in
            self.loginViewController.showInformationAlert(message: error.asCarlistyError().message!)
        }).disposed(by: bag)
    }

    func sendLoginRequest(email: String, password: String) -> Observable<User> {
        return Observable.create { observer in

            RestAPI().loginWithCredentials(email: email, password:
                password).asObservable().subscribe(onNext: { (loginResponse) in

                print(loginResponse)

                observer.onNext(loginResponse)
                observer.onCompleted()
            }, onError: { (error) in
                observer.onError(error)
                self.loginViewController.loginSubmitButton.hideLoading()
            }).disposed(by: self.bag)

            return Disposables.create()
        }
    }

    func isValidEmail(testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
