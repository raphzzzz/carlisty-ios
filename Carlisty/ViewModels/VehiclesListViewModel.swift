//
//  VehiclesListViewModel.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 16/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import RxSwift

class VehiclesListViewModel {
    let bag = DisposeBag()

    var vehicles: [Vehicle] = []

    var dealerId: String = ""

    var selectedVehicle: Vehicle?

    var isLoading = true

    func loadInformation() -> Observable<Bool> {
        return Observable.create { observer in

            RestAPI().getVehicles(dealerId: self.dealerId).asObservable().subscribe(onNext: { (vehicles) in
                self.isLoading = false
                self.vehicles = vehicles

                observer.onNext(true)
                observer.onCompleted()
            }, onError: { (error) in
                self.isLoading = false
                observer.onError(error)
            }).disposed(by: self.bag)

            return Disposables.create()
        }
    }

    func setSelectedVehile(index: Int) {
        self.selectedVehicle = vehicles[index]
    }
}
