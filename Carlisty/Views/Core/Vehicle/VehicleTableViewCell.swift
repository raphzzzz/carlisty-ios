//
//  VehicleTableViewCell.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 09/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import UIKit
import SDWebImage

class VehicleTableViewCell: UITableViewCell {

    @IBOutlet weak var model: UILabel!
    @IBOutlet weak var exteriorColor: UILabel!
    @IBOutlet weak var interiorColor: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var carThumbImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func fillInformation(vehicle: Vehicle) {
        model.text = vehicle.model
        exteriorColor.text = "Exterior Color: \(vehicle.exteriorColor)"
        interiorColor.text = "Interior Color: \(vehicle.interiorColor)"
        price.text = "Price: \(vehicle.listprice)"

        carThumbImage.sd_setImage(with: URL(string: String(format:
            CarlistyConstants().sirvDomainImageURL, "\(vehicle.views)", vehicle.sirvId, "001")),
                                  placeholderImage: UIImage(named: "profile_picture_placeholder.png"))
    }
}
