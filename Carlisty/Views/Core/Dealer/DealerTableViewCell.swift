//
//  DealerTableViewCell.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 09/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import UIKit

class DealerTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var profileImageView: RoundedImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func fillInformationFrom(dealer: Dealer) {
        let dealerCellViewModel = DealerCellViewModel()
        dealerCellViewModel.bindTo(cell: self, dealer: dealer)
    }
}
