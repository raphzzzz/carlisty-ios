//
//  VideoPlayerCustomView.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 11/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import UIKit
import BMPlayer

class VideoPlayerCustomView: BMPlayerControlView {

    /**
     Override if need to customize UI components
     */
    override func customizeUIComponents() {
        self.backButton.isHidden = true
        self.titleLabel.isHidden = true
        self.fullscreenButton.isHidden = true
    }
}
