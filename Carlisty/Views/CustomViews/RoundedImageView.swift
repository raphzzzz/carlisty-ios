//
//  RoundedImageView.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 09/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedImageView: UIImageView {

    override init(image: UIImage?) {
        super.init(image: image)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func layoutSubviews() {
        self.layer.cornerRadius = self.frame.size.height / 2
        self.clipsToBounds = true
    }
}
