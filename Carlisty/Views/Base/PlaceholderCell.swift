//
//  PlaceHolderCell.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 10/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import UIKit

class PlaceholderCell: UITableViewCell {

    fileprivate var animating: Bool = false

    override func layoutSubviews() {
        super.layoutSubviews()

        if animating {
            contentView.shimmer(with: 1)
        }
    }

    deinit {
        contentView.stopShimmering()
    }

    func animate() {
        animating = true
        contentView.shimmer(with: 1)
    }
}
