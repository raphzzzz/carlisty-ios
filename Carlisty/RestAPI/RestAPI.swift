//
//  RestAPI.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 09/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import RxSwift

class RestAPI {

    class func getFullPath(path: String) -> String {

        let fullUrl = String(format: "https://carlisty.herokuapp.com/%@", path)
        return fullUrl
    }

    func getDealers() -> Observable<[Dealer]> {
        return requestCarlistyAPI(urlString: RestAPI.getFullPath(path: "listDealers"),
                                   method: .post, parameters: ["token": CachingServices.currentUser!.token])
            .unwrapEntityCollection()
    }

    func getVehicles(dealerId: String) -> Observable<[Vehicle]> {
        return requestCarlistyAPI(urlString: RestAPI.getFullPath(path: "findVehicles"),
                                  method: .post, encoding: JSONEncoding.default, parameters: ["dealerID": dealerId])
            .unwrapEntityCollection()
    }

    func loginWithCredentials(email: String, password: String) -> Observable<User> {
        return requestCarlistyAPI(urlString: RestAPI.getFullPath(path: "login"),
                                  method: .post, encoding: JSONEncoding.default, parameters: [
                                    "email": email,
                                    "password": password
            ])
            .unwrapEntity()
    }
}
