//
//  CarlistyMessageModel.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 09/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import Foundation
import ObjectMapper

internal struct CarlistyMessageModel: Mappable {
    var message: String?
    var content: Any?

    init?(map: Map) { }

    mutating func mapping(map: Map) {
        message <- map["message"]
        content <- map["content"]
    }
}
