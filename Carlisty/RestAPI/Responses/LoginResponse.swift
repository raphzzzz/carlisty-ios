//
//  LoginResponse.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 25/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import ObjectMapper

class LoginResponse: Entity {

    var auth: Bool = false
    var token: String = ""
    var user: User?

    override func mapping(map: Map) {
        auth <- map["auth"]
        token <- map["token"]
        user <- map["user"]
    }
}
