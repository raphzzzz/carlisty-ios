//
//  CarlistyResponse.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 09/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift

/**
 Represents the return from a Carlisty REST api request.
 */
public enum CarlistyResponse: Error {
    /**
     The request completed sucefully and the message could be read.
     */
    case success(content: Any)

    /**
     The request completed with an error or the message could be processed. See the error code for more information.
     */
    case failure(code: CarlistyErrorCode, message: String?)

    /**
     Represents the errors returned by the processing a REST api.
     */
    public enum CarlistyErrorCode: Equatable {
        /**
         The response message from the server is not in the expected format and cannot be read.
         */
        case malformedCarlistyMessage

        /**
         The content from response message could not be serialized into specified type.
         */
        case serialization

        /**
         The user is not authorized to view the content.
         */
        case unauthorized

        /**
         The user could not connect to the server.
         */
        case connectionError

        /**
         Unknown error (must be avoided)
         */
        case unknown(originalError: Error)

    }

}

public func == (lhs: CarlistyResponse.CarlistyErrorCode, rhs: CarlistyResponse.CarlistyErrorCode) -> Bool {
    switch (lhs, rhs) {
    case (.unknown(let error1), .unknown(let error2)): return error1.localizedDescription == error2.localizedDescription
    case (.connectionError, .connectionError): return true
    case (.unauthorized, .unauthorized): return true
    case (.serialization, .serialization): return true
    case (.malformedCarlistyMessage, .malformedCarlistyMessage): return true
    default: return false
    }
}

extension ObservableType where Self.E == CarlistyResponse {

    public func unwrapJSON() -> Observable<[String: Any]> {

        return self.flatMap { response -> Observable<[String: Any]> in
            do {
                let value = try response.unwrapJSON()
                return Observable.just(value)
            } catch let error {
                return Observable.error(error)
            }

        }
    }

    /**
     Unwrap the contents of a CarlistyResponse into an observable if successul response, otherwise throws.
     */
    public func unwrap() -> Observable<String> {

        return self.flatMap { response -> Observable<String> in
            do {
                let value = try response.unwrap()
                return Observable.just(value)
            } catch let error {
                return Observable.error(error)
            }
        }
    }

    /**
     Returns an observable of the entity
     */
    public func unwrapEntity<T: Mappable>() -> Observable<T> {

        return self.flatMap { response -> Observable<T> in
            do {
                let value: T = try response.unwrapEntity()
                return Observable.just(value)
            } catch let error {
                return Observable.error(error)
            }

        }
    }

    /**
     Returns an observable of collection of the entity
     */
    public func unwrapEntityCollection<T: Mappable>() -> Observable<[T]> {

        return self.flatMap { response -> Observable<[T]> in
            do {
                let value: [T] = try response.unwrapEntityCollection()
                return Observable.just(value)
            } catch let error {
                return Observable.error(error)
            }

        }
    }
}

extension CarlistyResponse {

    /**
     Unwrap the contents of a CarlistyResponse into String.
     */
    public func unwrap() throws -> String {

        switch self {
        case .success(let content):
            guard let result = content as? String else {
                throw CarlistyResponse.failure(code: .serialization, message: "Could not unwrap content into dictionary")
            }

            return result

        case .failure:
            throw self
        }
    }

    /**
     Unwrap the contents of a CarlistyResponse into an entity.
     */
    public func unwrapEntity<T: Mappable>(_: T.Type) throws -> T {
        let result: T = try unwrapEntity()
        return result
    }

    /**
     Unwrap the contents of a CarlistyResponse into an entity.
     */
    public func unwrapEntity<T: Mappable>() throws -> T {

        switch self {
        case .success(let content):

            guard let mappedObject = Mapper<T>().map(JSONObject: content) else {
                throw CarlistyResponse.failure(code: .serialization, message: "Could not unwrap content into \(String(describing: T.self))")
            }
            return mappedObject
        case .failure(let code, let message):
            throw CarlistyResponse.failure(code: code, message: message)

        }
    }

    /**
     Unwrap the contents of a CarlistyResponse into an entity collection.
     */
    public func unwrapEntityCollection<T: Mappable>(_: T.Type) throws -> [T] {
        let result: [T] = try unwrapEntityCollection()
        return result
    }

    /**
     Unwrap the contents of a CarlistyResponse into an entity collection.
     */
    public func unwrapEntityCollection<T: Mappable>() throws -> [T] {
        switch self {
        case .success(let content):
            guard let mappedObjects = Mapper<T>().mapArray(JSONObject: content) else {
                throw CarlistyResponse.failure(code: .serialization, message: "Could not unwrap content into \(String(describing: T.self))")
            }
            return mappedObjects

        case .failure(let code, let message):
            throw CarlistyResponse.failure(code: code, message: message)
        }
    }

    /**
     Unwrap the contents of a CarlistyResponse into Dictionary.
     */
    public func unwrapJSON() throws -> [String: Any] {

        switch self {
        case .success(let content):

            guard let result = content as? [String: Any] else {
                throw CarlistyResponse.failure(code: .serialization, message: "Could not unwrap content into dictionary")
            }

            return result

        case .failure:
            throw self
        }
    }
}
