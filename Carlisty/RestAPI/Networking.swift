//
//  Networking.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 09/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Alamofire
import RxSwift
import SwiftyBeaver
import ObjectMapper

private let authorizationHttpHeader = "Authorization"

/**
 Asyncronously request a Carlisty rest API and return its content.
 
 - returns: A response with the contents of the Carlisty message if susseful, otherwise returns the error.
 
 - parameters:
 - url: The string url of the target request.
 - method: The http method used to do the request.
 - encoding: The method used to encode the request parameters, defalts to query string encoded.
 - headers: Any aditional header that will be sent with the request.
 - parameters: Any parameter that will be sent with the request.
 - authenticationHeaderProvider: A autorizaton header value generator if the request is to be authenticated.
 
 */
public func requestCarlistyAPI(
    urlString: String,
    method: HTTPMethod = .get,
    encoding: ParameterEncoding = URLEncoding.httpBody,
    headers: [String: String]? = nil,
    parameters: [String: Any]? = nil,
    session: SessionManager = SessionManager.default) -> Observable<CarlistyResponse> {

    return Observable.create { observer in

        let allHeaders: [String: String] = (headers != nil) ? headers! : [:]

        session.request(urlString, method: method, parameters: parameters, encoding: encoding, headers: allHeaders)
            .validate()
            .responseJSON { dataResponse in
                let response = extractContent(dataResponse: dataResponse)
                observer.onNext(response)
                observer.onCompleted()
        }

        return Disposables.create()
    }
}

private func extractContent(dataResponse: DataResponse<Any>) -> CarlistyResponse {
    SwiftyBeaver.debug("Requesting \(String(describing: dataResponse.request?.url?.absoluteString))")

    // Read the error from the request/message
    guard case .success(let rawValue) = dataResponse.result else {
        SwiftyBeaver.debug("Trying to extract error from CarlistyMessage...")
        return extractError(dataResponse: dataResponse)

    }
    // Response is a success but has no content AND we always expect some.
    // Should have content or should not use this serializer.
    if dataResponse.response?.statusCode == 204 {
        return .success(content: "")
    }

    // Message should have at least a MESSAGE and a CONTENT

    guard let carlistyMessage = Mapper<CarlistyMessageModel>().map(JSONObject: rawValue),
        let messageContent = carlistyMessage.content else {

            SwiftyBeaver.debug("Could not parse Carlisty message from response.")
            SwiftyBeaver.verbose(dataResponse)
            return .failure(code: .malformedCarlistyMessage,
                            message: NSLocalizedString("Could not parse the response into a valid Carlisty message", comment: String()))
    }

    SwiftyBeaver.verbose(dataResponse)
    return .success(content: messageContent)

}

private func extractError(dataResponse: DataResponse<Any>) -> CarlistyResponse {
    // Try to read the error from the message
    // !important: Do no trust dataResponse.result

    guard let data = dataResponse.data else {

        if let error = dataResponse.result.error as NSError? {
            switch error.code {
            case NSURLErrorCannotConnectToHost:
                return CarlistyResponse.failure(code: .connectionError, message: error.localizedDescription)
            default:
                return CarlistyResponse.failure(code: .unknown(originalError: error),
                                             message: error.localizedDescription)
            }
        } else {
            return CarlistyResponse.failure(code: .malformedCarlistyMessage, message: "Could not read Carlisty message")
        }
    }

    guard let json = try? JSONSerialization.jsonObject(with: data,
                                                       options: JSONSerialization.ReadingOptions.allowFragments),
        let carlistyMessage = Mapper<CarlistyMessageModel>().map(JSONObject: json),
        let errorMessage = carlistyMessage.message else {
            return CarlistyResponse.failure(code: .malformedCarlistyMessage, message: "Could not read Carlisty message")

    }

    return .failure(code: .malformedCarlistyMessage, message: errorMessage)
}
