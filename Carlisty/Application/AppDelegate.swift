//
//  AppDelegate.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 03/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import UIKit
import SwiftyBeaver
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let log = SwiftyBeaver.self

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        let barButtonItemAppearance = UIBarButtonItem.appearance()
        barButtonItemAppearance.setTitleTextAttributes([
            NSAttributedStringKey.foregroundColor: UIColor.clear], for: .normal)

        _ = UIBarButtonItem.appearance()
        barButtonItemAppearance.setTitleTextAttributes([
            NSAttributedStringKey.foregroundColor: UIColor.clear], for: .highlighted)

        IQKeyboardManager.sharedManager().enable = true

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
}
