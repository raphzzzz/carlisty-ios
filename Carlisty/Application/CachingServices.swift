//
//  CachingServices.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 30/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import AwesomeCache
import ObjectMapper

internal extension CachingServices {

    class var currentUser: User? {
        get {
            return CachingServices.get(User.self, key: "currentUser")
        }
        set {
            if let user = newValue {
                CachingServices.save(value: user, key: "currentUser")
            }
        }
    }
}

open class CachingServices {
    fileprivate static let cacheTTL: TimeInterval = 60 * 60 * 24 * 15

    fileprivate static let cache = try? Cache<NSString>(name: "carlisty.cache")

    fileprivate class func saveRaw(value: String, key: String) {
        CachingServices.cache!.setObject(value as NSString, forKey: key,
                                        expires: .date(Date().addingTimeInterval(cacheTTL)))
    }

    class func save<T: Mappable>(_ value: T, key: String?) {
        if key != nil && !(key?.isEmpty)! {
            save(value: value, key: "\(String(describing: T.self))_\(String(describing: key!))")
        } else {
            save(value: value, key: String(describing: T.self))
        }
    }

    fileprivate class func save<T: Mappable>(value: T, key: String) {
        let rawValue = Mapper<T>().toJSONString(value)!
        saveRaw(value: rawValue, key: key)
    }

    class func save<T: Mappable>(_ value: [T]) {
        save(value: value, key: String(describing: [T].self))
    }

    class func saveListWithKey<T: Mappable>(_ value: [T], key: String) {
        save(value: value, key: "\(String(describing: [T].self))_\(key)")
    }

    fileprivate class func save<T: Mappable>(value: [T], key: String) {
        let rawValue = Mapper<T>().toJSONString(value)!
        saveRaw(value: rawValue, key: key)
    }

    class func getListWithKey<T: Mappable>(_ value: T.Type, key: String) -> [T]? {
        return getAll(value, key: "\(String(describing: [T].self))_\(key)")
    }

    class func get<T>(_ value: T.Type, key: String?) -> T? where T: Mappable {
        if key != nil {
            return get(value, key: "\(String(describing: T.self))_\(key!)")
        } else {
            return get(value, key: String(describing: T.self))
        }
    }

    fileprivate class func get<T>(_: T.Type, key: String) -> T? where T: Mappable {

        if let cachedString = CachingServices.cache!.object(forKey: key) as String?,
            let data = cachedString.data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                return Mapper<T>().map(JSONObject: json)
            } catch {
                print("error")
            }
        }

        return nil

    }

    class func getAll<T: Mappable>(_ value: T.Type) -> [T]? {
        return getAll(value, key: String(describing: [T].self))
    }

    class func wipeData() {
        CachingServices.cache!.removeAllObjects()
    }

    fileprivate class func getAll<T: Mappable>(_: T.Type, key: String) -> [T]? {
        if let cachedString = CachingServices.cache!.object(forKey: key) as String?,
            let data = cachedString.data(using: String.Encoding.utf8) {
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    as? [[String: AnyObject]] {
                    return json.map { Mapper<T>().map(JSONObject: $0)! }
                }
            } catch {
                print("error")
            }
        }

        return []
    }
}
