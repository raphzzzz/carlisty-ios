//
//  CarlistyConfiguration.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 16/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import UIKit

struct CarlistyConstants {
    let sirvMp4Postfix = "Walkaround.mp4"

    let sirv360SpinPostfix = "360.spin"

    let sirvDomainURL = "https://media.carlisty.com/%@/%@/"

    let sirvDomainImageURL = "https://media.carlisty.com/%@/%@/%@.png?scale.width=500"

    let restApiURL = "https://carlisty.herokuapp.com"
}

struct CarlistyFonts {
    static func helveticaRegular(with size: CGFloat = 16) -> UIFont {
        return UIFont(name: "Helvetica-Regular", size: size)!
    }

    static func helveticaLight(with size: CGFloat = 20) -> UIFont {
        return UIFont(name: "Helvetica-Light", size: size)!
    }

    static func helveticaBold(with size: CGFloat = 20) -> UIFont {
        return UIFont(name: "Helvetica-Bold", size: size)!
    }

    static func entireScreenEmptyStateFont(with size: CGFloat = 16) -> UIFont {
        return UIFont(name: "Helvetica-Regular", size: size)!
    }
}
