//
//  MainViewController.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 03/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController {

    @IBOutlet weak var topConstraintScrollView: NSLayoutConstraint!

    @IBOutlet weak var homeIntroScrollPageControl: UIPageControl!

    @IBOutlet weak var homeIntroScrollView: UIScrollView!

    @IBOutlet weak var homeSignUpButton: UIButton!
    @IBOutlet weak var homeSignInButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        homeIntroScrollView.delegate = self

        setupTopConstraintForIphoneX()
        setupInterface()
        setupPageControl()
    }

    override func viewWillAppear(_ animated: Bool) {
        customizeNavBar()
        hideNavBar()
    }

    override func viewDidLayoutSubviews() {
        setupScrollView()
    }
}

extension MainViewController {

    func setupInterface() {
        homeSignUpButton.layer.cornerRadius = 8
    }

    func setupTopConstraintForIphoneX() {
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            topConstraintScrollView.constant = 110
        }
    }

    func setupPageControl() {
        homeIntroScrollPageControl.currentPage = 0
        homeIntroScrollPageControl.numberOfPages = 4
        homeIntroScrollPageControl.currentPageIndicatorTintColor = UIColor(hex: 0x007DBB)
        homeIntroScrollPageControl.pageIndicatorTintColor = UIColor.gray
    }

    func setupScrollView() {
        var offSetX: CGFloat = 0

        for pageIndex in 1...4 {
            homeIntroScrollView.addSubview(injectView(viewIndex: pageIndex, offSetX: offSetX))
            offSetX += homeIntroScrollView.frame.size.width
        }

        homeIntroScrollView.contentSize = CGSize(width: offSetX, height: homeIntroScrollView.frame.height)
        homeIntroScrollView.showsHorizontalScrollIndicator = false
        homeIntroScrollView.isPagingEnabled = true
    }

    func injectView(viewIndex: Int, offSetX: CGFloat) -> UIView {
        guard let page = UIView.fromNibString(nibName: "Page\(viewIndex)") else {
            return UIView()
        }

        page.frame = CGRect(x: offSetX, y: 0,
                            width: homeIntroScrollView.frame.size.width,
                            height: homeIntroScrollView.frame.height)

        return page
    }
}

extension MainViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        homeIntroScrollPageControl.currentPage = Int(pageNumber)
    }
}
