//
//  DetailsViewController.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 09/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import UIKit
import WebKit
import BMPlayer
import SDWebImage
import MessageUI

class DetailsViewController: BaseViewController {

    @IBOutlet weak var spinViewContainer: UIView!
    @IBOutlet weak var playerContainer: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var webView: WKWebView!

    let detailsViewModel = DetailsViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupInterface()
        setupWebView()
        setupPlayer()
    }

    override func viewDidLayoutSubviews() {
        setupScrollView()
    }
}

extension DetailsViewController {

    func setupInterface() {
        let button = UIButton(type: .custom)
        button.setTitle("Share", for: .normal)
        button.layer.cornerRadius = 5
        button.tintColor = .white
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(shareSms), for: UIControlEvents.touchUpInside)
        let barButton = UIBarButtonItem(customView: button)

        self.navigationItem.rightBarButtonItem = barButton
    }

    @objc func shareSms() {
        let controller = MFMessageComposeViewController()
        controller.body = "Message Body"
        controller.recipients = ["1234567890"]
        controller.messageComposeDelegate = self
        self.present(controller, animated: true, completion: nil)
    }

    func setupWebView() {
        let url = URL(string: detailsViewModel.getVehicle360ViewURL())!

        webView.load(URLRequest(url: url))
    }

    func setupPlayer() {
        let player = BMPlayer(customControlView: VideoPlayerCustomView())

        playerContainer.addSubview(player)

        player.snp.makeConstraints { (make) in
            make.top.equalTo(playerContainer.snp.top)
            make.left.equalTo(playerContainer.snp.left)
            make.right.equalTo(playerContainer.snp.right)
            make.bottom.equalTo(playerContainer.snp.bottom)
        }

        let asset = BMPlayerResource(url:
            URL(string: detailsViewModel.getVehicleMp4URL())!,
                                     name: "")
        player.setVideo(resource: asset)
        player.pause()
    }

    func setupScrollView() {
        var offSetX: CGFloat = 0

        let imagesIds = ["001", "063", "051", "045", "038", "026", "014", "008"]

        for imageId in imagesIds {
            let thumbImageView = UIImageView(frame:
                CGRect(x: offSetX, y: 0, width: scrollView.frame.size.height * 1.77, height:
                    scrollView.frame.size.height))

            let url = detailsViewModel.getThumbImageUrlFromIndex(imageId: imageId)
            thumbImageView.sd_setImage(with: URL(string: url),
                                       placeholderImage: UIImage(named: "profile_picture_placeholder.png"))

            offSetX += scrollView.frame.size.height * 1.77

            scrollView.addSubview(thumbImageView)
        }
        scrollView.contentSize = CGSize(width: offSetX, height: scrollView.frame.size.height)
    }
}

extension DetailsViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController,
                                      didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
}
