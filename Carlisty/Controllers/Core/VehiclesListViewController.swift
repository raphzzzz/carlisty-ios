//
//  VehiclesListViewController.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 09/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import UIKit

class VehiclesListViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    let vehiclesListViewModel = VehiclesListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        loadInformation()
    }
}

extension VehiclesListViewController {

    func setupTableView() {
        tableView.register(UINib(nibName:
            String(describing: VehicleTableViewCell.self), bundle: nil),
                           forCellReuseIdentifier: String(describing: VehicleTableViewCell.self))

        tableView.register(UINib(nibName: String(describing:
            VehiclePlaceholderTableViewCell.self), bundle: nil), forCellReuseIdentifier:
            String(describing: VehiclePlaceholderTableViewCell.self))

        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }

    func loadInformation() {
        vehiclesListViewModel.loadInformation().asObservable().subscribe(onNext: { _ in
            self.tableView.reloadData()
        }, onError: { _ in
            self.tableView.reloadData()
        }).disposed(by: vehiclesListViewModel.bag)
    }
}

extension VehiclesListViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVehicleDetail" {
            if let viewController = segue.destination as? DetailsViewController {
                viewController.detailsViewModel.setSelectedVehicle(vehicle: vehiclesListViewModel.selectedVehicle!)
            }
        }
    }
}

extension VehiclesListViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehiclesListViewModel.isLoading ? 10 : vehiclesListViewModel.vehicles.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var identifier = ""

        if vehiclesListViewModel.isLoading {
            identifier = String(describing: VehiclePlaceholderTableViewCell.self)
            guard let placeHolderCell = tableView.dequeueReusableCell(
                withIdentifier: identifier, for: indexPath) as? VehiclePlaceholderTableViewCell
                else {
                    return PlaceholderCell()
            }

            placeHolderCell.animate()

            return placeHolderCell
        } else {
            identifier = String(describing: VehicleTableViewCell.self)
            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: identifier, for: indexPath) as? VehicleTableViewCell
                else {
                    return UITableViewCell()
            }

            cell.fillInformation(vehicle: vehiclesListViewModel.vehicles[indexPath.row])

            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        vehiclesListViewModel.setSelectedVehile(index: indexPath.row)
        performSegue(withIdentifier: "showVehicleDetail", sender: nil)
    }
}
