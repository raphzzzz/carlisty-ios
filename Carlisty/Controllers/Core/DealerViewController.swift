//
//  HomeViewController.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 09/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import UIKit
import RxSwift
import Foundation
import ObjectMapper
import DZNEmptyDataSet

class DealerViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!

    let dealerViewModel = DealerViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        loadInformation()
    }
}

extension DealerViewController {

    func setupTableView() {
        tableView.register(UINib(nibName:
            String(describing: DealerTableViewCell.self), bundle: nil),
                           forCellReuseIdentifier: String(describing: DealerTableViewCell.self))

        tableView.register(UINib(nibName:
            String(describing: DealerPlaceholderTableViewCell.self), bundle: nil),
                           forCellReuseIdentifier: String(describing: DealerPlaceholderTableViewCell.self))

        tableView.setup(viewController: self)
        tableView.setupEmptySource(viewController: self)
    }

    func loadInformation() {
        dealerViewModel.loadInformation().asObservable().subscribe(onNext: { _ in
            self.tableView.reloadData()
        }, onError: { error in
            self.showInformationAlert(message: error.asCarlistyError().message!)
            self.tableView.reloadData()
        }).disposed(by: dealerViewModel.bag)
    }
}

extension DealerViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVehicleForDealer" {
            if let viewController = segue.destination as? VehiclesListViewController {
                guard let dealerSelected = dealerViewModel.dealerSelected else {
                    return
                }

                viewController.vehiclesListViewModel.dealerId = dealerSelected.id
            }
        }
    }
}

extension DealerViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        dealerViewModel.setSelectedDealerWithIndex(index: indexPath.row)
        performSegue(withIdentifier: "showVehicleForDealer", sender: nil)
    }
}

extension DealerViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dealerViewModel.isLoading ? 10 : dealerViewModel.dealers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var identifier = ""

        if dealerViewModel.isLoading {
            identifier = String(describing: DealerPlaceholderTableViewCell.self)

            guard let placeHolderCell = tableView.dequeueReusableCell(
                withIdentifier: identifier, for: indexPath) as? DealerPlaceholderTableViewCell else {
                return PlaceholderCell()
            }

            placeHolderCell.animate()

            return placeHolderCell
        } else {
            identifier = String(describing: DealerTableViewCell.self)

            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: identifier, for: indexPath) as? DealerTableViewCell else {
                return UITableViewCell()
            }

            cell.fillInformationFrom(dealer: dealerViewModel.dealers[indexPath.row])

            return cell
        }
    }
}

// MARK: DZNEmptyDataSet DataSource
extension DealerViewController: DZNEmptyDataSetSource {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "white_logo")
    }

    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {

        let attrs = [NSAttributedStringKey.strokeColor: UIColor.gray,
                     NSAttributedStringKey.font: CarlistyFonts.helveticaRegular()]

        return NSAttributedString(string: "Empty data", attributes: attrs)
    }
}

// MARK: DZNEmptyDataSet Delegate
extension DealerViewController: DZNEmptyDataSetDelegate {

    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }

    func imageTintColor(forEmptyDataSet scrollView: UIScrollView!) -> UIColor! {
        return UIColor.gray
    }
}
