//
//  BaseViewController.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 04/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import UIKit
import RxSwift
import SwiftyBeaver

class BaseViewController: UIViewController {

    let log = SwiftyBeaver.self

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showNavBar()
        customizeNavBar()
    }

    func customizeNavBar() {
        let navigationBarAppearace = UINavigationBar.appearance()

        navigationBarAppearace.tintColor = UIColor.white
        navigationBarAppearace.barTintColor = UIColor(hex: 0x007DBB)
        navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }

    func showNavBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    func hideNavBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    func showInformationAlert(title: String = "Error", message: String = "Error") {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

        self.present(alert, animated: true)
    }
}
