//
//  LoginViewController.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 04/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginSubmitButton: RequestButton!

    var loginViewModel: LoginViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupInterface()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showNavBar()
    }
}

extension LoginViewController {

    func setupInterface() {
        loginSubmitButton.layer.cornerRadius = 8
        loginViewModel = LoginViewModel(loginViewController: self)
    }

    @IBAction func loginClick(_ sender: Any) {
        loginViewModel.login(email: emailTextField.text!, password: passwordTextField.text!)
    }
}
