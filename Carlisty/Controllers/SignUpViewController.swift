//
//  SignUpViewController.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 04/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController {

    @IBOutlet weak var signUpSubmitButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupInterface()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showNavBar()
    }
}

extension SignUpViewController {

    func setupInterface() {
        signUpSubmitButton.layer.cornerRadius = 8
    }
}
