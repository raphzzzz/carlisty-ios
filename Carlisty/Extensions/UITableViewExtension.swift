//
//  UITableViewExtension.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 30/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import UIKit
import DZNEmptyDataSet

extension UITableView {
    func setupEmptySource(viewController: UIViewController) {
        self.emptyDataSetDelegate = viewController as? DZNEmptyDataSetDelegate
        self.emptyDataSetSource = viewController as? DZNEmptyDataSetSource
    }

    func setup(viewController: UIViewController) {
        self.dataSource = viewController as? UITableViewDataSource
        self.delegate = viewController as? UITableViewDelegate
    }
}
