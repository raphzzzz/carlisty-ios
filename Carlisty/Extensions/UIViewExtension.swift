//
//  UIViewExtension.swift
//  Carlisty
//
//  Created by Raphael Pedrini Velasqua on 03/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    class func fromNib<T: UIView>() -> T? {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as? T
    }

    class func fromNibString<T: UIView>(nibName: String) -> T? {
        return Bundle.main.loadNibNamed(nibName, owner: nil, options: nil)![0] as? T
    }

    func shimmer(with time: CFTimeInterval) {
        subviews.forEach { (subview) in
            subview.layer.mask?.removeAllAnimations()

            let transparency: CGFloat = 0.5
            let gradientWidth: CGFloat = 80.0

            let gradientMask = CAGradientLayer()
            gradientMask.frame = CGRect(x: -subview.frame.origin.x, y: 0, width: self.bounds.width, height: subview.frame.height)

            let gradientSize = gradientWidth/self.bounds.width
            let gradientColor = UIColor(white: 1, alpha: transparency)
            let startLocations = [0, gradientSize/2, gradientSize]
            let endLocations = [(1 - gradientSize), (1 - gradientSize/2), 1]
            let animation = CABasicAnimation(keyPath: "locations")

            gradientMask.colors = [gradientColor.cgColor, UIColor.white.cgColor, gradientColor.cgColor]
            gradientMask.locations = startLocations as [NSNumber]?
            gradientMask.startPoint = CGPoint(x: 0 - (gradientSize * 2), y: 0.5)
            gradientMask.endPoint = CGPoint(x: 1 + gradientSize, y: 0.5)

            subview.layer.mask = gradientMask

            animation.fromValue = startLocations
            animation.toValue = endLocations
            animation.repeatCount = HUGE
            animation.duration = time

            gradientMask.add(animation, forKey: nil)
        }
    }

    func stopShimmering() {
        subviews.forEach { (subview) in
            subview.layer.mask?.removeAllAnimations()
        }
    }
}

extension UIColor {
    convenience init(hex: Int, alpha: Double = 1.0) {
        self.init(red: CGFloat((hex>>16)&0xFF)/255.0, green: CGFloat((hex>>8)&0xFF)/255.0, blue: CGFloat((hex)&0xFF)/255.0, alpha: CGFloat(255 * alpha) / 255)
    }
}
