//
//  LoginViewModelTests.swift
//  CarlistyTests
//
//  Created by Raphael Pedrini Velasqua on 25/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import XCTest

@testable import Carlisty
class LoginViewModelTests: XCTestCase {

    let viewModel = LoginViewModel(loginViewController: LoginViewController())

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testInit_LoginEmailIsValid() {
        let email = "raphaelpedrini@gmail.com"

        XCTAssertEqual(viewModel.isValidEmail(testStr: email), true)
    }

    func testInit_LoginEmailIsInvalid() {
        let email = "raphaelpedrinigmail.com"

        XCTAssertEqual(viewModel.isValidEmail(testStr: email), false)
    }
}
