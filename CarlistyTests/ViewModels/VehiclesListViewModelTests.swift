//
//  VehiclesListViewModelTests.swift
//  CarlistyTests
//
//  Created by Raphael Pedrini Velasqua on 19/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import Carlisty
class VehiclesListViewModelTests: XCTestCase {

    let vehiclesListViewModel = VehiclesListViewModel()

    var vehicle: Vehicle?

    override func setUp() {
        super.setUp()

        let mock: [String: Any] = [
            "dealerID": "5acb39d487d7380004e63860",
            "stocknumber": "#1154",
            "sirvId": "WDDJK7DA1HF049622",
            "views": 1
        ]

        if let vehicle = Mapper<Vehicle>().map(JSON: mock) {
            self.vehicle = vehicle
        }
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testInit_SetSelectedVehicle() {
        vehiclesListViewModel.selectedVehicle = self.vehicle
        XCTAssertNotNil(vehiclesListViewModel.selectedVehicle)
    }
}
