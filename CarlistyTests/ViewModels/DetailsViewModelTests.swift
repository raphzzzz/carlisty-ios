//
//  DetailsClassTest.swift
//  CarlistyTests
//
//  Created by Raphael Pedrini Velasqua on 19/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import Carlisty
class DetailsViewModelTests: XCTestCase {

    let detailsViewModel = DetailsViewModel()

    let carlistyConstants = CarlistyConstants()

    override func setUp() {
        super.setUp()

        let mock: [String: Any] = [
            "dealerID": "5acb39d487d7380004e63860",
            "stocknumber": "#1154",
            "sirvId": "WDDJK7DA1HF049622",
            "views": 1
        ]

        if let vehicle = Mapper<Vehicle>().map(JSON: mock) {
            detailsViewModel.setSelectedVehicle(vehicle: vehicle)
        }
    }

    override func tearDown() {
        super.tearDown()
    }

    func testInit_initViewController() {
        detailsViewModel.initViewController(detailsVc: DetailsViewController())

        XCTAssertNotNil(detailsViewModel.detailsVc)
    }

    func testInit_SetSelectedVehicleWithVehicle() {
        XCTAssertNotNil(detailsViewModel.vehicleSelected)
    }

    func testInit_GetSelectedVehicle360URL() {
        XCTAssertEqual(detailsViewModel.getVehicle360ViewURL(),
                       String(format: carlistyConstants.sirvDomainURL +
            carlistyConstants.sirv360SpinPostfix, "1", "WDDJK7DA1HF049622"))
    }

    func testInit_GetSelectedVehicleMp4URL() {
        XCTAssertEqual(detailsViewModel.getVehicleMp4URL(),
                       String(format: carlistyConstants.sirvDomainURL +
            carlistyConstants.sirvMp4Postfix, "1", "WDDJK7DA1HF049622"))
    }

    func testInit_GetSelectedVehicleImageFromIndex() {
        let index = 0

        let imagesIds = ["001", "063", "051", "045", "038", "026", "014", "008"]

        XCTAssertEqual(detailsViewModel.getThumbImageUrlFromIndex(imageId: imagesIds[index]),
                       String(format: carlistyConstants.sirvDomainImageURL, "1", "WDDJK7DA1HF049622", imagesIds[index]))
    }
}
