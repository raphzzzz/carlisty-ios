//
//  DealerCellViewModelTests.swift
//  CarlistyTests
//
//  Created by Raphael Pedrini Velasqua on 24/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import Carlisty

class DealerCellViewModelTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testInit_BindDealerToCell() {
        let jsonDictionary: [String: Any] = ["name": "Raphael Pedrini",
                                             "address": "Flat 9, Example Address"]
        guard let dealer = Mapper<Dealer>().map(JSON: jsonDictionary) else {
            return XCTFail("dealer is nil")
        }

        let bundle = Bundle(for: DealerTableViewCell.self)
        guard let cell = bundle.loadNibNamed(String(describing: DealerTableViewCell.self),
                                             owner: nil)?.first as? DealerTableViewCell else {
            return XCTFail("DealerTableViewCell nib did not contain a UIView")
        }

        cell.fillInformationFrom(dealer: dealer)

        XCTAssertEqual(cell.nameLabel.text, dealer.name)
        XCTAssertEqual(cell.addressLabel.text, dealer.address)
    }
}
