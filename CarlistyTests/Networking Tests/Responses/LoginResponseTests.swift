//
//  LoginResponseTests.swift
//  CarlistyTests
//
//  Created by Raphael Pedrini Velasqua on 25/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import XCTest
import SwiftyJSON
import ObjectMapper

@testable import Carlisty
class LoginResponseTests: XCTestCase {

    var bundle: Bundle!

    var path: URL!

    var response: LoginResponse!

    override func setUp() {
        super.setUp()

        bundle = Bundle(for: DealerClassTests.self)

        path = bundle.url(forResource: "login_response", withExtension: "json")

        do {
            let data = try Data(contentsOf: path)
            let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments)

            if let dictionary = object as? [String: AnyObject] {
                response = Mapper<LoginResponse>().map(JSON: dictionary)
            } else {
                XCTFail("Error generating json dictionary")
            }
        } catch {
            XCTFail("Try error")
        }
    }

    override func tearDown() {
        super.tearDown()
    }

    func testInit_LoginResponseMockIsNotNil() {
        if let path = bundle.path(forResource: "login_response", ofType: "json") {
            XCTAssertNotNil(path)
        } else {
            XCTFail("No JSON file")
        }
    }

    func testInit_LoginResponseWithAuthAndTokenFromMock() {
        XCTAssertNotNil(response)
        XCTAssertEqual(response.auth, true)
        XCTAssertEqual(response.token, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9")
    }
}
