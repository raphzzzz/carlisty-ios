//
//  ResponseTests.swift
//  CarlistyTests
//
//  Created by Raphael Pedrini Velasqua on 18/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import XCTest
import Alamofire
import Foundation
import SwiftyJSON

class ResponseTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    func testThatResponseReturnsSuccessResultWithValidData() {
        // Given
        let urlString = "https://carlisty.herokuapp.com/dealers"
        let expectation = self.expectation(description: "request should succeed")

        var response: DefaultDataResponse?

        // When
        Alamofire.request(urlString).response { (resp) in
            response = resp
            expectation.fulfill()
            //if let json = resp.data {
                //let data = try? JSON(data: json)
            //}
        }

        waitForExpectations(timeout: 5000, handler: nil)

        // Then
        XCTAssertNotNil(response?.request)
        XCTAssertNotNil(response?.response)
        XCTAssertNotNil(response?.data)
        XCTAssertNil(response?.error)
    }
}
