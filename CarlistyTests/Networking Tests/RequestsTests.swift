//
//  RequestsTests.swift
//  CarlistyTests
//
//  Created by Raphael Pedrini Velasqua on 18/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import XCTest
import Foundation
import Alamofire

class RequestsTests: XCTestCase {

    var urlString = ""

    var manager: SessionManager!

    override func setUp() {
        super.setUp()
    }

    func testDealersRequest() {
        // Given
        let urlString = "https://carlisty.herokuapp.com/dealers"

        // When
        let request = Alamofire.request(urlString)

        XCTAssertNotNil(request.request)
        XCTAssertEqual(request.request?.httpMethod, "GET")
        XCTAssertNil(request.response)
    }

    func testVehiclesRequest() {
        // Given
        let urlString = "https://carlisty.herokuapp.com/vehicles"

        // When
        let request = Alamofire.request(urlString)

        XCTAssertNotNil(request.request)
        XCTAssertEqual(request.request?.httpMethod, "GET")
        XCTAssertNil(request.response)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
}
