//
//  DealersAPIsTests.swift
//  CarlistyTests
//
//  Created by Raphael Pedrini Velasqua on 18/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import Foundation
import XCTest
import Alamofire

class DealersAPIsTests: XCTestCase {

    var urlString = ""

    var manager: SessionManager!

    override func setUp() {
        super.setUp()

        // Clear out cookies
        let cookieStorage = HTTPCookieStorage.shared
        cookieStorage.cookies?.forEach { cookieStorage.deleteCookie($0) }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
}
