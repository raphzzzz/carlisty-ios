//
//  UserClassTests.swift
//  CarlistyTests
//
//  Created by Raphael Pedrini Velasqua on 25/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import Carlisty
@testable import Pods_Carlisty

class UserClassTests: XCTestCase {

    var bundle: Bundle!

    var path: URL!

    var user: User!

    override func setUp() {
        super.setUp()

        bundle = Bundle(for: UserClassTests.self)

        path = bundle.url(forResource: "user_mock", withExtension: "json")

        do {
            let data = try Data(contentsOf: path)
            let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments)

            if let dictionary = object as? [String: AnyObject] {
                user = Mapper<User>().map(JSON: dictionary)
            } else {
                XCTFail("Error generating json dictionary")
            }
        } catch {
            XCTFail("Try error")
        }
    }

    override func tearDown() {
        super.tearDown()
    }

    func testInit_UserMockFromLocalJSONFile() {
        if let path = bundle.path(forResource: "user_mock", ofType: "json") {
            XCTAssertNotNil(path)
        } else {
            XCTFail("No JSON file")
        }
    }

    func testInit_UserFromMock() {
        XCTAssertNotNil(user)
        XCTAssertEqual(user?.name, "Raphael Pedrini")
        XCTAssertEqual(user?.email, "raphaelpedrini@gmail.com")
    }
}
