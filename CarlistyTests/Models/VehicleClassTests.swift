//
//  VehicleClassTests.swift
//  CarlistyTests
//
//  Created by Raphael Pedrini Velasqua on 19/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import Carlisty
class VehicleClassTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testInit_VehicleWithJSON() {

        let mock: [String: Any] = [
            "dealerID": "5acb39d487d7380004e63860",
            "stocknumber": "#1154",
            "sirvId": "WDDJK7DA1HF049622"
        ]

        let vehicle = Mapper<Vehicle>().map(JSON: mock)

        XCTAssertNotNil(vehicle)
        XCTAssertEqual(vehicle?.dealerID, "5acb39d487d7380004e63860")
        XCTAssertEqual(vehicle?.stocknumber, "#1154")
        XCTAssertEqual(vehicle?.sirvId, "WDDJK7DA1HF049622")
    }
}
