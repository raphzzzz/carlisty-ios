//
//  DealerClassTests.swift
//  CarlistyTests
//
//  Created by Raphael Pedrini Velasqua on 17/04/2018.
//  Copyright © 2018 Carlisty. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import Carlisty
@testable import Pods_Carlisty
class DealerClassTests: XCTestCase {

    var bundle: Bundle!

    var path: URL!

    var dealer: Dealer!

    override func setUp() {
        super.setUp()

        bundle = Bundle(for: DealerClassTests.self)

        path = bundle.url(forResource: "dealer_mock", withExtension: "json")

        do {
            let data = try Data(contentsOf: path)
            let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments)

            if let dictionary = object as? [String: AnyObject] {
                dealer = Mapper<Dealer>().map(JSON: dictionary)
            } else {
                XCTFail("Error generating json dictionary")
            }
        } catch {
            XCTFail("Try error")
        }
    }

    override func tearDown() {
        super.tearDown()
    }

    func testInit_DealerMockFromLocalJSONFile() {
        if let path = bundle.path(forResource: "dealer_mock", ofType: "json") {
            XCTAssertNotNil(path)
        } else {
            XCTFail("No JSON file")
        }
    }

    func testInit_DealerFromMock() {
        XCTAssertNotNil(dealer)
        XCTAssertEqual(dealer?.name, "Fort Lauderdale Motors")
        XCTAssertEqual(dealer?.address, "54th Street - Example Address")
    }
}
